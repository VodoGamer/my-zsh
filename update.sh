#!/bin/zsh

script_path=`dirname $0`
zsh_custom_path=$HOME/.config/zsh

cp -r $script_path/default* $zsh_custom_path/
