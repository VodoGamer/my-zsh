alias tmux_fullstack_dev="
tmux new -s fullstack_dev -d
tmux split-window -v -t fullstack_dev:1.1
tmux rename-window -t fullstack_dev:1 servers
tmux send-keys -t fullstack_dev:1.1 'poetry run uvicorn api:app' Enter
tmux send-keys -t fullstack_dev:1.2 'npm run dev' Enter

tmux new-window -t fullstack_dev:2
tmux rename-window -t fullstack_dev:2 pgcli
tmux send-keys -t fullstack_dev:2 'pgcli $(basename "$PWD")' Enter

tmux new-window -t fullstack_dev:3

tmux rename-window -t fullstack_dev:3 poetry
tmux send-keys -t fullstack_dev:3 'poetry shell' Enter
"
