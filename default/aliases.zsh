# system shortcuts
alias ls="ls -1F --color=auto"
alias l="ls -CFa"
alias la="ls -Fla"

# external shortcuts
alias v="nvim"
alias bat="batcat"

# commands
cpp_run () {
    clang++ -o /tmp/cpp.out $1 && /tmp/cpp.out
}

cpp_lint () {
    clang-format -i $1
}
