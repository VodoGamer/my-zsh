#!/bin/zsh

script_path=`dirname $0`
zsh_custom_path=$HOME/.config/zsh

mkdir -p $zsh_custom_path
{ echo -n "export ZSH_CUSTOM=\"$zsh_custom_path\" \n"; cat $HOME/.zshrc; } > $HOME/.zshrc.tmp
mv $HOME/.zshrc{.tmp,}
$script_path/update.sh
